<?php

namespace AppBundle\Entity;

/**
 * Jugador
 */
class Jugador
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var integer
     */
    private $id_equipo;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var int
     */
    private $dorsal;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Jugador
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set dorsal
     *
     * @param integer $dorsal
     *
     * @return Jugador
     */
    public function setDorsal($dorsal)
    {
        $this->dorsal = $dorsal;

        return $this;
    }

    /**
     * Get dorsal
     *
     * @return int
     */
    public function getDorsal()
    {
        return $this->dorsal;
    }


    /**
     * Set idEquipo
     *
     * @param integer $idEquipo
     *
     * @return Jugador
     */
    public function setIdEquipo($idEquipo)
    {
        $this->id_equipo = $idEquipo;

        return $this;
    }

    /**
     * Get idEquipo
     *
     * @return integer
     */
    public function getIdEquipo()
    {
        return $this->id_equipo;
    }
}
