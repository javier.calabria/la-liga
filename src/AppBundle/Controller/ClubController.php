<?php

namespace AppBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\Routing\Annotation\Route;

// API
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Club;
// use AppBundle\Entity\Jugador;

// error return
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ClubController extends FOSRestController
{
    /**
     * @Rest\Get("/club")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Club')->findAll();
       
        if ($restresult === null) {
            return new View("No se han encontrado Clubs", Response::HTTP_NOT_FOUND);
        }
        return $restresult;

    }


    /**
    * @Rest\Get("/club/{id}")
    */
    public function idAction($id)
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Club')->find($id);
        
        if ($restresult === null) {
            return new View("Club no encontrado", Response::HTTP_NOT_FOUND);
        }

        return $this->render(
                "club.html.twig",
                $restresult
        );
    }


    /**
     * @Rest\Get("/home")
     */
    public function getListado()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Club')->findAll();
        
        if ($restresult === null) {
            return new View("No se han encontrado Clubs", Response::HTTP_NOT_FOUND);
        }

        return $this->render(
                "base.html.twig",
                $restresult
        );

    }
}
