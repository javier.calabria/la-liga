import React from 'react';
import ReactDOM from 'react-dom';

import Menu from './Menu';
import Club from './Club';

require("../css/style.css");

const appContext = React.createContext(window.location);

ReactDOM.render(
  <Club />,
  document.getElementById('js-clubs-wrapper')
);

ReactDOM.render(
  <Menu />,
  document.getElementById('app-menu')
);
