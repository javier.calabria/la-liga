<?php
namespace Tests\AppBundle\Util;

use AppBundle\Entity\Jugador;
use PHPUnit\Framework\TestCase;

class jugadorTest extends TestCase
{
    public function testAdd()
    {
        $jugador = new Jugador();
        $jugador->setNombre('javi');
        $result = $jugador->getNombre();

        // assert that your jugador added the numbers correctly!
        $this->assertEquals('javi', $result);
    }
}