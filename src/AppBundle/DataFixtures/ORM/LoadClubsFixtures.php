<?php

namespace AppBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Club;
use AppBundle\Entity\Jugador;

class LoadClubsFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

    	$manager->getConnection()->exec('ALTER TABLE Club AUTO_INCREMENT = 1');

    	// .rand(1, 100)
        $clubs = array(
        	array(0, 'Atlético de Madrid'),
			array(2, 'Barcelona'),
			array(3, 'Real Madrid'),
			array(4, 'Sevilla'),
			array(5, 'Valencia')
		);

        foreach ($clubs as $key => $value) {
    		$club = new Club();
	        // $club->setId($key);
	        $club->setNombre($value[1]);

	        /*$club->setSubFamily('Octopodinae');
	        $club->setSpeciesCount(rand(100, 99999));*/
	        $manager->persist($club);
        	$manager->flush();
        }
    }
}