import React from 'react';
import Club from './Club';
import { Fragment } from 'react' // react version > 16.0

class Jugador extends React.Component {
  constructor(props) {
    super(props);

    this.state = { "jugadores": [], "idequipo": 0 , "nombre_equipo": "", "loaded": false, "num_jugadores":0, "is_insert":false, "txtGrabar": "Grabar", "sinjugadores":false };

    // para poder cambiar state
    this.nuevoJugador = this.nuevoJugador.bind(this);
    this.grabarJugador = this.grabarJugador.bind(this);
    this.cancelar = this.cancelar.bind(this);
    // this.equipoSinJugadores = this.equipoSinJugadores.bind(this);
  }

  getDerivedStateFromProps(props, state) {
    if (this.props.idequipo!=null && props.idequipo != this.props.idequipo) 
    {
      return this.getListadoJugadores();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.idequipo != this.props.idequipo || prevState.num_jugadores != this.state.num_jugadores) {
      this.getListadoJugadores();
      this.setState({ loaded: false});
    }
  }

  getListadoJugadores() {
    var equipo = this.props.idequipo;

    this.setState(
      (state) => {
        // Important: read `state` instead of `this.state` when updating.
        return { "idequipo": equipo, "nombre_equipo": this.props.nombre, "is_insert": false}
      }
    );

    var url = '/GET/jugadores';
    if (equipo!=0) {
      url += '/equipo/' + equipo;
    }

    fetch(url)
      .then(response => {
      return response.json();
    })
    .then(d => {
      this.setState({ jugadores: d.jugadores, loaded: true });
    })
    /*.catch(e => {
        // this.equipoSinJugadores();
    });
    */
  }

  componentDidMount() {
    this.getListadoJugadores();
  }

  equipoSinJugadores() {

    // , jugadores:null
    this.setState({ sinjugadores: true, loaded: true });
    return (
      <div>
        Este equipo aún no tiene jugadores dados de alta. <br/>
        <button id="btnNuevoJugador" onClick={this.nuevoJugador} className="btn btn-primary">Nuevo Jugador</button>
      </div>
    );
  }

  volver() {
    listarEquipos();
  }

  cancelar() {
    this.setState({ is_insert: false });
  }

  render() {

    if(!this.state.loaded) {
      return (
        <div className="spinner-border" role="status">
          <span className="sr-only">Cargando...</span>
        </div>
      );
    }

    if(this.state.sinjugadores) {
      return (
        <div>
          Este equipo aún no tiene jugadores dados de alta. <br/>
          <button id="btnNuevoJugador" onClick={this.nuevoJugador} className="btn btn-primary">Nuevo Jugador</button>
        </div>
      );

    }

    var mis_jugadores = this.state.jugadores;
    if (this.state.loaded && mis_jugadores!=null && mis_jugadores!='') {
      mis_jugadores = JSON.parse(mis_jugadores);

      // Mostramos el form de alta
      if ( this.state.is_insert ) {
        return (
          <div>
            {this.renderForm()}
          </div>
        );
      } else {
        var equipo_old = 0;
        var div2, divCabecera, divFila;
        return (
        <div>
          <div className="row">
            <button type="button" className="btn btn-info" onClick={this.volver}> Volver a Equipos</button>
          </div>
          <div className="row">
          <table className="table" border="1">
            <thead className="thead-dark">

            {this.props.idequipo != 0 ? (
              <tr>
                <th colSpan="2">{'Plantilla del ' + this.props.nombre}</th>
                <th>
                <button id="btnNuevoJugador" onClick={this.nuevoJugador} className="btn btn-primary">Nuevo Jugador</button>
                </th>
              </tr>
            ):null}

            {this.props.idequipo == 0 ? (
              <tr>
                  <th>EQUIPO</th>
                  <th>JUGADOR</th>
                  <th>DORSAL</th>
                </tr>
            ):(

              <tr>
                <th>ID</th>
                <th>JUGADOR</th>
                <th>DORSAL</th>
              </tr>
            ) }            
             
            </thead>
            
            {
                mis_jugadores.map(((jugador, index) => {

                  divCabecera = null;
                  if ( equipo_old!=jugador.id_equipo && this.props.idequipo == 0 ) {
                    divCabecera = (
                      <tr key={'cab_' + jugador.id}>
                        <th colSpan="3">
                          <h1>{ jugador.nombre_equipo } </h1>
                        </th>
                      </tr>
                    )
                  }

                  divFila = (
                    <tr key={'props_' + jugador.id}>
                      <th>
                        &nbsp; #{ jugador.id }
                      </th>

                      <td>{jugador.nombre}</td><td>{jugador.dorsal}</td>
                    </tr>
                  )

                  equipo_old = jugador.id_equipo;

                  return (
                    <tbody key={'boby_' + jugador.id}>
                      {divCabecera}
                      {divFila}
                    </tbody>
                  );
                }

              ))
            }
            </table>
            </div>
          </div>
        );
      } //if alta
    } else {
      return (
        <div>Cargando Jugadores</div>
      );
    }
  }

  nuevoJugador() {
    this.setState({ is_insert: true, sinjugadores:false });
    return (
      <div>Cargando nuevoJugador</div>
    );
  }

  grabarJugador() {
    this.setState({ is_insert: false, num_jugadores: 1 });
    event.preventDefault();

    const data = new FormData(event.target);
    var mform = document.getElementById('newPlayer');
    data.set('nombre', mform.nombre.value);
    data.set('dorsal', mform.dorsal.value);
    data.set('id_equipo', mform.id_equipo.value);

    var url = "/POST/jugador";
    fetch(url, {method: 'POST', body: data, })
      .then(response => {
        return response.json();
      })
      .then(d => {
    }).bind(this);

  }

  handleChange() {
    // event.preventDefault();
  }
  
  renderForm() {
    return (
      <form id="newPlayer" onSubmit={this.grabarJugador} data-toggle="validator">
        <div>
          <h1 className="modal-title">Nuevo Jugador</h1>
          <h6>{this.props.nombre}</h6>
        </div>
        <div className="form-group">
          <label htmlFor="nombre">Nombre Jugador</label>
          <input id="nombre" name="nombre" type="text" className="form-control" placeholder="Escriba el nombre del Jugador" required>
          </input>
        </div>
        <div className="form-group">
          <label htmlFor="dorsal">Dorsal Jugador</label>
          <input id="dorsal" name="dorsal" type="number" className="form-control" placeholder="Escriba el dorsal del Jugador" required>
          </input>
          <input id="id_equipo" name="id_equipo" type="hidden" className="form-control" value={this.props.idequipo} onChange={this.handleChange}>
          </input>
        </div>
        <button className="btn btn-success">Grabar</button>&nbsp;
        <button type="button" className="btn btn-warning" onClick={this.cancelar}>Cancelar</button>
      </form>
    );
  }

}

function listarEquipos() {
  ReactDOM.render(
    <Club name="Javi"  />,
    document.getElementById('js-clubs-wrapper')
  );
}

export default Jugador; // Don’t forget to use export default!