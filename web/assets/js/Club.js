import React from 'react';
import Jugador from './Jugador';

class Club extends React.Component {

  constructor(props) {
    super(props);
    this.state = { "seconds": 0, "clubs": [], "render":"", "equipo_id":0 , "row_active": false};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, compName, equipo){
    this.setState(state => ({
      render: compName,
      equipo_id: equipo.id, 
      row_active: !this.state.row_active
    }));
    listarJugadores(equipo);
  }

  componentDidMount() {
    var url = "/GET/clubs";
    fetch(url)
    .then(response => {
      return response.json();
    })
    .then(d => {
      this.setState({ clubs: d.clubs });
    })
  }

  render() {
    var mis_clubs = this.state.clubs;
    if (mis_clubs!=null && mis_clubs!='') {

      mis_clubs = JSON.parse(mis_clubs);
     
      return (
        <div>
          <div className="row">
            <h2>Equipos</h2>
          </div>
          <div className="row">
            <p>Pulsa sobre el botón para ver la plantilla del Equipo</p>
          </div>
          <table className="table">
          <thead className="thead-dark">
            <tr>
              <th>ID</th>
              <th>EQUIPO</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          {
            mis_clubs.map(((equipo, index) =>
              <tr key={equipo.id} className={this.state.row_active ? 'thead-light': null}>
                <td>{equipo.id}</td>
                <td>{equipo.nombre}</td>
                <td>
                  <button key={equipo.id} className="btn btn-primary"  onClick={() => this.handleClick(this, 'listaJugadores', equipo)}>
                  Jugadores
                  </button>
                </td>
              </tr>
            ))
          }
          </tbody>
          </table>
          </div>
      );

    } else {
      return (
        <div className="spinner-border" role="status">
          <span className="sr-only">Cargando Equipos...</span>
        </div>
      );
    }
  }
          
}

function listarJugadores(equipo) {
  ReactDOM.render(
    <Jugador idequipo={equipo.id} nombre={equipo.nombre} />,
    document.querySelector('#js-clubs-wrapper')
  );
}

export default Club; // Don’t forget to use export default!