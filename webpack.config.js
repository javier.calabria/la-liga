var Encore = require('@symfony/webpack-encore');
 
Encore
    // Directorio donde se almacenarán los assets ya compilados.
    .setOutputPath('web/build/')
 
    .setPublicPath('/build')

    .addEntry('bootstrap', './web/assets/css/bootstrap.css')

    .addEntry('style', './web/assets/css/style.css')
    
    .addEntry('js/app', './web/assets/js/app.js')

    // Habilitar el mapeo de recursos en Desarrollo.
    .enableSourceMaps(!Encore.isProduction())
 
    // // Borra el contenido del directorio /web/build antes de volver a compilar una nueva versión.
    .cleanupOutputBeforeBuild()
 
    // // Muestra una notificación cuando se ha finalizado la compilación.
    .enableBuildNotifications()
 
    // // Activa React
    .enableReactPreset()

    // console jcp
    // .enableSingleRuntimeChunk()
    .disableSingleRuntimeChunk()

    // "build/images/logo.png": "/build/images/logo-laliga-web-90a.png",
    // "build/images/logo-laliga-web-90a-black.png": "/build/images/logo-laliga-web-90a-black.png",
    
    .copyFiles({
       from: './web/assets/images',

       // optional target path, relative to the output dir
       //to: 'images/[path][name].[ext]',

       // if versioning is enabled, add the file hash too
       //to: 'images/[path][name].[hash:8].[ext]',

       // only copy files matching this pattern
       //pattern: /\.(png|jpg|jpeg)$/
   })
;
 
// Exporta la configuración final
module.exports = Encore.getWebpackConfig();