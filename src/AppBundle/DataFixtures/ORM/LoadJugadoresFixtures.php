<?php

namespace AppBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Club;
use AppBundle\Entity\Jugador;

class LoadJugadoresFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

    	$clubs = array(
        	array(1, 'Atlético de Madrid'),
			array(2, 'Barcelona'),
			array(3, 'Real Madrid'),
			array(4, 'Sevilla'),
			array(5, 'Valencia'),
		);

    	$jugadores = array(
			 [1, 'Godín', 		'2', 'defensa']
			,[1, 'Filipe Luis', '3', 'defensa']
			,[1, 'Santiago Arias', '4', 'defensa']
			,[1, 'Thomas', 		'5', 'centrocampista']
			,[1, 'Koke', 		'6', 'centrocampista']
			,[1, 'Saúl', '8', 'centrocampista']
			,[1, 'Griezmann', '7', 'delantero']
			,[1, 'Kalinic', '9', 'delantero']
			,[1, 'Correa', '10', 'delantero']
		 	,[1, 'Diego Costa', '19', 'delantero']

			,[2, 'Ter Stegen', '1', 'portero']
			,[2, 'Nelson Semedo', '2', 'defensa']
			,[2, 'Piqué', '3', 'defensa']
			,[2, 'Jean-Clair Todibo', '6', 'defensa']
			,[2, 'Rakitic', '4', 'centrocampista']
			,[2, 'Busquets', '5', 'centrocampista']
			,[2, 'Coutinho', '7', 'centrocampista']
			,[2, 'Luis Suárez', '9', 'delantero']
			,[2, 'Messi', '10', 'delantero']
			,[2, 'Ousmane Dembélé', '11', 'delantero']
			,[2, 'Malcom', '14', 'delantero']

			,[3, 'Keylor Navas', '1', 'portero']
			,[3, 'Carvajal', '2', 'defensa']
			,[3, 'Jesús Vallejo', '3', 'defensa']
			,[3, 'Sergio Ramos', '4', 'defensa']
			,[3, 'Kroos', '8', 'centrocampista']
			,[3, 'Modric', '10', 'centrocampista']
			,[3, 'Casemiro', '14', 'centrocampista']
			,[3, 'Mariano', '7', 'delantero']
			,[3, 'Benzema', '9', 'delantero']
			,[3, 'Bale', '11', 'delantero']
			,[3, 'Vinicius Junior', '28', 'delantero']
		);

		foreach ($jugadores as $key => $value) {

        	$jugador = new Jugador();
        	$jugador->setIdEquipo( intval($value[0])  );
        	$jugador->setNombre( $value[1] );
        	$jugador->setDorsal( $value[2] );
    		
    		$manager->persist($jugador);
			$manager->flush();
	    }
    }
}