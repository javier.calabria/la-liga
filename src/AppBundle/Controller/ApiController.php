<?php

// src/AppBundle/Controller/ApiController.php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Club;
use AppBundle\Entity\Jugador;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiController extends FOSRestController
{
 
  /**
   * @Route("/GET/clubs")
   */
  public function getClubs()
  {

    $response = new JsonResponse();

    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    $em = $this->getDoctrine()->getManager();
    $clubs =  $em->getRepository('AppBundle:Club');

    $query = $em->createQuery('SELECT c
        FROM AppBundle:Club c
        ORDER BY c.nombre ASC'
    );

    $clubs = $query->getResult();
    $response->setStatusCode(200);
    $response->setData(array(
        'response' => 'success',
        'clubs' => $serializer->serialize($clubs, 'json')
    ));

    return $response;

  }

  /**
  * @Route("/GET/jugadores/equipo/{id_equipo}")
  */
  public function getJugadoresEquipo($id_equipo)
  {
    return $this->getJugadores($id_equipo);
  }

  /**
  * @Route("/GET/jugadores")
  */
  public function getJugadores($id_equipo = null)
  {
    $response = new JsonResponse();
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    $em = $this->getDoctrine()->getManager();
    $equipos =  $em->getRepository('AppBundle:Jugador');
    
    if ( $id_equipo != null)
    {
      $parameters = array(
          'p_equipo_id' => $id_equipo
      );
      $query = $em->createQuery(
        'SELECT j.id, j.nombre, j.dorsal, c.nombre nombre_equipo, c.id id_equipo
        FROM AppBundle:Jugador j
        INNER JOIN AppBundle:Club c
        WHERE j.id_equipo = :p_equipo_id
        AND c.id = j.id_equipo
        ORDER BY j.nombre ASC'
      );
      $query->setParameters($parameters);

    } else {

      $query = $em->createQuery(
        'SELECT j.id, j.nombre, j.dorsal, c.nombre nombre_equipo, c.id id_equipo
        FROM AppBundle:Jugador j
        LEFT JOIN AppBundle:Club c
        WHERE c.id = j.id_equipo
        ORDER BY c.nombre, j.nombre ASC'
      );
    }

    $equipos = $query->getResult();

    $response->setStatusCode(200);
    $response->setData(array(
        'response' => 'success',
        'jugadores' => $serializer->serialize($equipos, 'json')
    ));

    return $response;
  }

  /**
  * @Route("/POST/jugador")
  * @Method({"POST", "HEAD"})
  */
  public function postJugador(Request $request)
  {
      $response = new JsonResponse();
      
      $nombre = $_POST['nombre'];
      $dorsal = $_POST['dorsal'];
      $id_equipo = $_POST['id_equipo'];

      $body = 'vacio';
      if (isset($_POST['body'])) {
        $body = $_POST['body'];
      }

      $equipo = array(
        'nombre' => $nombre,
        'dorsal' => $dorsal,
        'id_equipo' => $id_equipo,
      ); 

      $response->setData(array(
        'response' => $nombre. ' ' . $dorsal. ' ' . $id_equipo,
        'equipo' => $equipo,
        'body' => $body,
      ));

      $jugador = new Jugador();
      $form = $this->createForm('AppBundle\Form\JugadorType', $jugador);

      $form->handleRequest($request);

      $jugador->setNombre( $nombre );
      $jugador->setIdEquipo( $id_equipo );
      $jugador->setDorsal( $dorsal );

      $response = new JsonResponse();
      $rdo = 'error';

      if (true) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($jugador);
          $em->flush();

          $rdo = 'success';
      } else {
        dump($form->getData());
        dump($form->getErrors(true, false));
        die('<br/>prueba');
      }

      $response->setData(array(
            'response' => $rdo,
            'equipo' => $equipo,
      ));

      return $response;

  }

    
}