import React from 'react';
import Club from './Club';
import Jugador from './Jugador';

class Menu extends React.Component {

  constructor(props) {
    super(props);
    this.state = { "activeEquipos": true, "activeJugadores": false }

    this.equiposClick = this.equiposClick.bind(this);
    this.jugadoresClick = this.jugadoresClick.bind(this);
  }

  equiposClick(id, e){
    event.preventDefault();
    
    this.setState(state => ({
      activeEquipos: !this.state.activeEquipos,
      activeJugadores: false
    }));

    todosEquipos();
  }

  jugadoresClick(id, e){
    event.preventDefault();

    this.setState(state => ({
      activeJugadores: !this.state.activeJugadores,
      activeEquipos: false
    }));

    todosJugadores();
  }

  render() {

    return (
      <React.Fragment>
        <button className={ 'btn ' + (this.state.activeEquipos ? 'btn-primary active' : 'btn-light' ) } onClick={() => this.equiposClick()}>
          Equipos
        </button>
        &nbsp;
        <button className={ 'btn ' + (this.state.activeJugadores ? 'btn-primary active' : 'btn-light' ) } onClick={() => this.jugadoresClick()}>
          Jugadores
        </button>
      </React.Fragment>
    );
  }
}

function todosJugadores() {
  ReactDOM.render(
  <Jugador name="Javi" idequipo="0" nombre="" />,
  document.getElementById('js-clubs-wrapper')
  );
}

function todosEquipos() {
  ReactDOM.render(
    <Club />,
    document.getElementById('js-clubs-wrapper')
  );
}

export default Menu; // Don’t forget to use export default!